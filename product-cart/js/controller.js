// tạo 1 biến gobal để nhận giá trị danh sách các phone
let newPhones = []
// biến để chứa và render ra danh sách phone cart
let carts = JSON.parse(localStorage.getItem("phoneList")) || []
renderCartList(carts)



export function renderPhoneList(phones) {
  // dùng spreadoperator để [...value]
  newPhones = [...phones]
  const htmls = phones.map((phone, index) => {
    return `
         <div class="flex items-center">
        <div class="group relative w-[200px] h-[300px] bg-white rounded-[20px] transition-all duration-300 hover:w-[240px] hover:h-[460px] hover:duration-300 shrink-0">
            <div class="flex items-center justify-between px-[16px] pt-2">
              <i class="text-black">${phone.type}</i>
              <h4 class="text-lime-400">In Stock</h4>
            </div>
            <div class="w-[192px] h-[92[px] mx-auto py-4">
              <img src="${phone.img}" alt="">
            </div>
            <div class="text-white bg-[#2e2f33] p-3 rounded-[20px] w-full absolute group-hover:bottom-[0]">
              <div class="flex items-center justify-between">
                <div>${phone.name}</div>
                <button onclick="this.classList.toggle('text-red-500')">
                <i id="heart" class="las la-heart transition-all duration-300"></i>
                </button>
              </div>
              <div class="text-white ease-in-out overflow-hidden hidden group-hover:block">
                <div class="">
                  <h4 class="font-normal text-sm">Màn hình : ${phone.screen}</h4>
                  <h5 class="text-sm">Camera trước : ${phone.frontCamera}</h5>
                  <h5 class="text-sm">Camera sau : ${phone.backCamera}</h5>
                  <p class="text-sm my-[6px]">${phone.desc}</p>
                </div>
                <div class="flex justify-between mt-3">
                  <span>Giá : ${phone.price}$</span>
                  <div id="addProduct-${index}" class="p-[5px] border-[1px] border-white rounded-full text-[11px] font-medium cursor-pointer hover:bg-white hover:text-black" onclick="addItem(this, ${index})">
                    Add
                    <i class="las la-angle-right"></i>
                  </div>
                  <div id="quantity-${index}" class="hidden">
                  <button id="reduced" onclick="countNumber(this, 'sub', ${index})">
                    <i class="p-[1px] rounded-full border-[1px] font-semibold hover:bg-white hover:text-black las la-angle-left"></i>
                  </button>
                    <span id="number-${index}"></span>
                  <button id="increase" onclick="countNumber(this, 'add', ${index})">
                    <i class="p-[1px] rounded-full border-[1px] font-semibold hover:bg-white hover:text-black las la-angle-right"></i>
                  </button>
                  </div>
                </div>
              </div>
            </div>
        </div>
     </div>
        `;
  });
  document.getElementById("cellPhones").innerHTML = htmls.join('');
}


function addItem(_this, index) {
  // khi ấn add thì ẩn add và hiện số lượng lên
  _this.classList.add("hidden");
  _this.nextElementSibling.classList.remove("hidden");

  // Khi ấn add thì tăng số lượng lên 1
  const isValue = checkCartExits(newPhones[index].id)
  if(isValue) {
    var cartItem = {product:newPhones[index], quantity: 0}
    carts.push(cartItem)
    renderCartList(carts)
    localStorage.setItem('phoneList', JSON.stringify(carts))
  } 
  countNumber(null, "add", index)
}
window.addItem = addItem;



function checkCartExits(id) {
  for(let i = 0; i<carts.length; i++) {
    if(carts[i].product.id == id) {
      return false
    }
  }
  return true
}
window.checkCartExits = checkCartExits

function checkId(id) {
  for(let i = 0; carts.length; i++) {
    if(carts[i].product.id == id) {
      return i
    }
  }
}
window.checkId = checkId

    
// lưu số lượng xuống localStorage
document.getElementById("soLuong").innerHTML = localStorage.getItem("soLuongSp")


function countNumber(_this, type, index) {  
  let numberEl = document.getElementById(`number-${index}`);
  let number = numberEl.innerText * 1;
  
  // so id với nhau lấy ra được index để khi khi tăng lên 1 thì tăng đúng cái của nó
  let indexCart = checkId(newPhones[index].id)

  if (type == "add") {
    numberEl.innerText = ++number;
    carts[indexCart].quantity +=1
    // newPhones[indexCart].quantity += 1
  } else {
    numberEl.innerText = --number;
    carts[indexCart].quantity -= 1
    // newPhones[indexCart].quantity -= 1
    // khi ấn xuống = 0 thì hiện add và ẩn số lượng đi
    if (number <= 0) {
      document.getElementById(`addProduct-${index}`).classList.remove("hidden");
      document.getElementById(`quantity-${index}`).classList.add("hidden");
    }
  }
    // cập nhật khi click để thay đổi số lượng
      localStorage.setItem('phoneList', JSON.stringify(carts))
      renderCartList(carts)
      quantityCart()
}

function quantityCart() {
  let totalProduct = 0
  for(let i = 0; i<carts.length; i++) {
    totalProduct += carts[i].quantity
  }
  localStorage.setItem("soLuongSp", totalProduct)
  document.getElementById("soLuong").innerHTML = totalProduct
}
window.quantityCart = quantityCart

window.countNumber = countNumber;

function countNumberCart(_this, type, index) {
  let soLuongEl = document.getElementById("soLuong")
  let soLuong = soLuongEl.innerText * 1
  if (type == "add") {
    carts[index].quantity +=1
    soLuongEl.innerText = ++soLuong
  } else {
    carts[index].quantity -= 1
    soLuongEl.innerText = --soLuong
  }
  if(carts[index].quantity <= 0) {
    carts.splice(index, 1)
    renderCartList(carts)
    localStorage.setItem('phoneList', JSON.stringify(carts))
  }
  localStorage.setItem('phoneList', JSON.stringify(carts))
  renderCartList(carts)
  localStorage.setItem("soLuongSp", soLuong)
  
}

window.countNumberCart = countNumberCart


function renderCartList(cartList) {
  const htmls = cartList.map((item, index) => {
    return `
    <div class="my-[20px]">
    <div class="flex items-center justify-between mx-[30px] p-[16px] border-b-[1px] border-b-white hover:bg-white hover:bg-opacity-25 duration-200">
      <img class="w-[40px] h-[40px]" src="${item.product.img}" alt="">
      <h4>${item.product.name}</h4>      
      <div>
      <button id="reduced" onclick="countNumberCart(this, 'sub', ${index})">
      <i class="p-[1px] rounded-full border-[1px] font-semibold hover:bg-white hover:text-black las la-angle-left"></i>
      </button>
      <span id="numberCart-${index}">${item.quantity}</span>
      <button id="increase" onclick="countNumberCart(this, 'add', ${index})">
      <i class="p-[1px] rounded-full border-[1px] font-semibold hover:bg-white hover:text-black las la-angle-right"></i>
      </button>
      </div>   
      <div>$${item.product.price * item.quantity}</div>
      <button onclick="removeItemCart(${index})" class=""><i class="las la-trash"></i></button>
    </div>
  </div>
    `
  })
  document.getElementById("cartList").innerHTML = htmls.join('')
}

function removeItemCart(index) {
  carts.splice(index, 1)
  renderCartList(carts)
  localStorage.setItem("phoneList", JSON.stringify(carts))
}

window.removeItemCart = removeItemCart


function handleEvents() {
  const closeCart = document.getElementById("closeCart")
  const cartOverlay = document.getElementById("cartOverlay")
  const btnCart = document.getElementById("btnCart")
  // khi click thì đóng giỏ hàng lại
  closeCart.addEventListener("click", function(e) {
    // khi click ẩn overlay
    cartOverlay.classList.add("hidden")
    // cho cart sang phải
    cartList.parentElement.classList.remove("right-[0]")
  })
  // khi click vào giỏ hàng thì hiện lên
  btnCart.addEventListener("click", function(e) {
    // hiện giỏ hàng lên
    cartOverlay.classList.remove("hidden")
    // xét cho giỏ hàng sang phải và ẩn nó đi
    cartList.parentElement.classList.add("right-[0]")
  })

  // khi click ra phía bên ngoài giỏ hàng thì cũng ẩn đi
  cartOverlay.addEventListener("click", function(e) {
    // khi click ẩn overlay
    cartOverlay.classList.add("hidden")
    // cho cart sang phải
    cartList.parentElement.classList.remove("right-[0]")
  })
}
handleEvents()
