import { renderPhoneList } from "./controller.js";
const BASE_URL = `https://63c9e750d0ab64be2b495a0b.mockapi.io/Products`;

function fetchPhoneList() {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data)
      renderPhoneList(res.data);
    })
    .catch((err) => {
      console.log("err", err);
    });
}

// fetchPhoneList();


function choisePhone() {

  let typePhone = document.getElementById("typePhone");
  if (typePhone.value == "none") {
    fetchPhoneList()
  } else if (typePhone.value == "iphone") {
    axios({
      url: `${BASE_URL}`,
      method: "GET",
    })
      .then((res) => {
        let iphones = res.data.filter(item => {
          if(item.type.toLowerCase() == typePhone.value) {
            return item
          }
        })
        renderPhoneList(iphones)
        
      })
      .catch((err) => {
        console.log("err", err);
      });
  }else {
    axios({
      url: `${BASE_URL}`,
      method: "GET",
    })
      .then((res) => {
        let samSungs = res.data.filter(item => {
          if(item.type.toLowerCase() == typePhone.value) {
            return item
          }
        })
        renderPhoneList(samSungs)
      })
      .catch((err) => {
        console.log("err", err);
      });
  }
}

choisePhone()
window.choisePhone = choisePhone;
