// kiểm tra chung
export function kiemTraChung(value, idErr) {
    let output = value.trim()
    if(output == "") {
        document.getElementById(idErr). innerHTML = "Vui lòng nhập trường này"
        return false
    }else {
        document.getElementById(idErr). innerHTML = ""
        return true
    }
}

// Kiểm tra chữ
export function kiemTraChu(value, idErr) {
    let re = /^\D+$/
    let output = re.test(value)
    if(output) {
        document.getElementById(idErr).innerHTML = ""
        return true
    }else {
        document.getElementById(idErr).innerHTML = "Trường này phải là chữ"
        return false
    }
}

export function kiemTraSo(value, idErr) {
    let re = /^\d+$/
    let output = re.test(value) 
    if(output) {
        document.getElementById(idErr).innerHTML = ""
        return true
    }else {
        document.getElementById(idErr).innerHTML = "Trường này phải là số"
        return false
    }
}