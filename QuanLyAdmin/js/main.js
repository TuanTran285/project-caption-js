import { capNhatThongTinLenForm, layThongTinSanPham, renderProduct, validate } from "./controller.js";
const BASE_URL = `https://63c9e750d0ab64be2b495a0b.mockapi.io/adminProduct`;
function fetchProductList() {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      renderProduct(res.data);
    })
    .catch((err) => {
      console.log("err", err);
    });
}
fetchProductList();

function themSanPham() {
  let sanPham = layThongTinSanPham();
  let isValid = validate(sanPham)
  if(isValid) {
    axios({
      url: BASE_URL,
      method: "POST",
      data: sanPham,
    })
    .then((res) => {
      fetchProductList();
      $("#exampleModalCenter").modal("hide");
    })
    .catch((err) => {
      console.log("err", err);
    });
  }
}

window.themSanPham = themSanPham;

function xoaSanPham(id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchProductList();
    })
    .catch((err) => {
      console.log("err", err);
    });
}
window.xoaSanPham = xoaSanPham;

function suaSanPham(id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      capNhatThongTinLenForm(res.data)
      $("#exampleModalCenter").modal("show");
      document.getElementById("themSanPham").disabled = true
      document.getElementById("capNhatBtn").disabled = false
    })
    .catch((err) => {
      console.log("err", err);
    });
  }
  window.suaSanPham = suaSanPham

  function capNhatSanPham() {
    let sanPham = layThongTinSanPham()
    let isValid = validate(sanPham)
    if(isValid) {
      axios({
        url: `${BASE_URL}/${sanPham.id}`,
        method: "PUT",
        data: sanPham
      }).then(res => {
        $("#exampleModalCenter").modal("hide");
        fetchProductList()
    }).catch(err => {
      console.log('err', err)
    })
  }
}
window.capNhatSanPham = capNhatSanPham