import { kiemTraChung, kiemTraChu, kiemTraSo } from "./validate.js";

export function renderProduct(products) {
    let contentProduct = products.reverse().map(product => {
        return `<tr>
            <td>${product.id}</td>
            <td>${product.tenSanPham}</td>
            <td>${product.giaSanPham}</td>
            <td>${product.hinhAnh}</td>
            <td>${product.moTa}</td>
            <td><button class="btn btn-danger" onclick="xoaSanPham(${product.id})">Xóa</button>
            <button class="btn btn-info" onclick="suaSanPham(${product.id})">Sửa</button></td>
        </tr>`
    })
    document.getElementById("tBodySanPham").innerHTML = contentProduct
}

export function layThongTinSanPham() {
    let id = document.getElementById('maSp').value
    let tenSanPham = document.getElementById('tenSp').value
    let giaSanPham = document.getElementById('giaSp').value
    let hinhAnh = document.getElementById('hinhSp').value
    let moTa = document.getElementById('moTaSp').value
    return {
        id,
        tenSanPham,
        giaSanPham,
        hinhAnh,
        moTa
    }
}

export function capNhatThongTinLenForm(product) {
    document.getElementById('maSp').value = product.id
    document.getElementById('tenSp').value = product.tenSanPham
    document.getElementById('giaSp').value = product.giaSanPham
    document.getElementById('hinhSp').value = product.hinhAnh
    document.getElementById('moTaSp').value = product.moTa
}


document.getElementById("themMoiBtn").addEventListener("click", function(e) {
    document.getElementById("themSanPham").disabled = false
    document.getElementById("capNhatBtn").disabled = true
})


export function validate(sanPham) {
    let isValid = true
    isValid = kiemTraChung(sanPham.tenSanPham, "tbTen") && kiemTraChu(sanPham.tenSanPham, "tbTen")
    isValid = isValid & (kiemTraChung(sanPham.giaSanPham, "tbGia") && kiemTraSo(sanPham.giaSanPham, "tbGia"))
    isValid = isValid & (kiemTraChung(sanPham.hinhAnh, "tbAnh"))
    isValid = isValid & (kiemTraChung(sanPham.moTa, "tbMoTa"))
    return isValid
}
window.validate = validate